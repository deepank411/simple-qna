app.service("dataService", function($q){

   var service = {};
   service.getData = getData;
   service.getAnswers = getAnswers;

   return service;

   function getData(){
      var deferred = $q.defer();

      var data = [{
         q_id: 1,
   		question: 'Longest river in the world?',
   		options: [
   			{"id": "op11", "text": 'Nile'},
   			{"id": "op12", "text": 'Amazon'},
   			{"id": "op13", "text": 'Ganges'}
   		]
   	}, {
         q_id: 2,
   		question: 'Highest mountain in the world?',
   		options: [
   			{"id": "op21", "text": 'Mt. Kilimanjaro'},
   			{"id": "op22", "text": 'Mt. Everest'},
   			{"id": "op23", "text": 'Mt. Kanchunjanga'}
   		]
   	}, {
         q_id: 3,
   		question: 'Largest coffee producing country in the world?',
   		options: [
   			{"id": "op31", "text": 'India'},
   			{"id": "op32", "text": 'Brazil'},
   			{"id": "op33", "text": 'Venezuela'},
            {"id": "op34", "text": "Columbia"}
   		]
   	}, {
         q_id: 4,
   		question: 'City of canals?',
   		options: [
   			{"id": "op41", "text": 'Venice'},
   			{"id": "op42", "text": 'Copenhagen'}
   		]
   	}, {
         q_id: 5,
   		question: 'Land of rising sun?',
   		options: [
   			{"id": "op51", "text": 'China'},
   			{"id": "op52", "text": 'South Korea'},
   			{"id": "op53", "text": 'Japan'}
   		]
   	}
   ];

      deferred.resolve(data);
      return deferred.promise;
   }

   function getAnswers(){
      var deferred = $q.defer();

      var answers = [{
         q_id: 1,
         a_id: 'op11'
      }, {
         q_id: 2,
         a_id: 'op22'
      }, {
         q_id: 3,
         a_id: 'op32'
      }, {
         q_id: 4,
         a_id: 'op41'
      }, {
         q_id: 5,
         a_id: 'op53'
      }];

      deferred.resolve(answers);
      return deferred.promise;
   }
});
