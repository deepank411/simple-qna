var app = angular.module("finomenaApp", ['ui.router', 'chart.js']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){
	$stateProvider
	.state("home", {
		url: "/home",
		controller: 'homeCtrl',
		templateUrl: "templates/home.html"
	})
	.state("result", {
		url: "/result",
		controller: 'resultCtrl',
		templateUrl: "templates/result.html"
	});
	$urlRouterProvider.otherwise("home");
}]);
