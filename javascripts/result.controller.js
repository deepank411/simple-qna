app.controller("resultCtrl", ['$scope', '$rootScope', 'dataService', '$state', function($scope, $rootScope, dataService, $state){

   if(!$rootScope.userAnswers){
      $state.go("home");
   }
   else{
      $scope.resultsArray = [];

      var correct = 0;
      var incorrect = 0;

      dataService.getAnswers().then(function(actualAnswers){
         dataService.getData().then(function(resp){
            angular.forEach(resp, function(key, value){
               var res = $.grep(actualAnswers, function(e){ return e.q_id == key.q_id; });
               var correct_ans = $.grep(key.options, function(e) { return e.id == res[0]['a_id']});
               var res1 = $.grep($rootScope.userAnswers, function(e){ return e.q_id == key.q_id});
               var user_ans = $.grep(key.options, function(e){ return e.id == res1[0].a_id});

               console.log(correct_ans[0].id);
               console.log(user_ans[0].id);

               var obj = {
                  q_id: key.q_id,
                  question: key.question,
                  correctAnswer: correct_ans[0].text,
                  userAnswer: user_ans[0].text
               }

               if(correct_ans[0].id == user_ans[0].id){
                  correct++;
                  obj['status'] = 'Correct';
               }
               else{
                  incorrect++;
                  obj['status'] = 'Incorrect';
               }

               $scope.labels = ["Correct Answers", "Incorrect Answers"];
               $scope.data = [correct, incorrect];

               $scope.resultsArray.push(obj);
            })
         });
   	});
   }
}])
