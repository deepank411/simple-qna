app.controller("homeCtrl", ['$rootScope', '$scope', 'dataService', '$state', function($rootScope, $scope, dataService, $state){

	$scope.question_index = 0;
	$scope.results = [];

	$scope.showBlock = false;

	dataService.getData().then(function(response){
		$scope.questions = response;
	});

	$scope.enterName = function(){
		if($scope.name == undefined){
			Materialize.toast("Please enter you name", 3000);
		}
		else {
			$rootScope.userName = $scope.name;
			$scope.showBlock = true;
		}
	}

	var userAnswers = [];

	$scope.submitAnswer = function(ques_id, ans_id){
		userAnswers.push({q_id: ques_id, a_id: ans_id});
		$rootScope.userAnswers = userAnswers;
		$scope.question_index++;
		if($scope.question_index >= $scope.questions.length){
			$state.go("result");
		}
	}

	$scope.createRipple = function(id, event){

		$(".ripple").remove();

		var posX = $("div#"+id).offset().left,
		posY = $("div#"+id).offset().top,
		buttonWidth = $("div#"+id).width(),
		buttonHeight =  $("div#"+id).height();

		$("div#"+id).prepend("<span class='ripple'></span>");

		// Make it round!
		if(buttonWidth >= buttonHeight) {
			buttonHeight = buttonWidth;
		} else {
			buttonWidth = buttonHeight;
		}

		var x = event.pageX - posX - buttonWidth / 2;
		var y = event.pageY - posY - buttonHeight / 2;


		// Add the ripples CSS and start the animation
		$(".ripple").css({
			width: buttonWidth,
			height: buttonHeight,
			top: y + 'px',
			left: x + 'px',
			background: '#1BBC9B'
		}).addClass("rippleEffect");

	}

}]);
