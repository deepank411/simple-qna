# Assignment - Demo

This repo contains the code of the Finomena Assignment.

### Steps to run the code:
- `(sudo) npm install`
- `python -m SimpleHTTPServer 8080`
- Navigate to localhost:8080 on browser.

The application will open on localhost:3000 automatically.

### Technologies Used
- Angular.js
- NPM
- HTML
- CSS (SCSS)
- Jquery